#include <stream9/unique_array/unique_array.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

namespace json { using namespace stream9::json; }

using stream9::unique_array;

BOOST_AUTO_TEST_SUITE(unique_array_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            unique_array<int> a;
        }

        BOOST_AUTO_TEST_CASE(with_size_)
        {
            unique_array<int> a(5);
        }

        BOOST_AUTO_TEST_CASE(with_size_and_value_)
        {
            unique_array<int> a(5, 100);
        }

        BOOST_AUTO_TEST_CASE(with_iterator_)
        {
            std::vector<int> a1 { 1, 2, 3, 4, 5 };

            unique_array<int> a2(a1.begin(), a1.end());
        }

        BOOST_AUTO_TEST_CASE(with_initializer_list_)
        {
            unique_array<int> a1 { 1, 2, 3, 4, 5 };
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(assignment_with_ilist_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        a1 = { 4, 5, 6, };

        auto v = json::value_from(a1);
        json::array expected { 4, 5, 6 };
        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_SUITE(iterator_)

        BOOST_AUTO_TEST_CASE(begin_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.begin()), T::iterator>);

            auto it = a1.begin();

            BOOST_TEST(*it == 1);
        }

        BOOST_AUTO_TEST_CASE(end_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.end()), T::iterator>);

            auto i1 = a1.begin();
            auto i2 = a1.end();

            BOOST_TEST(std::distance(i1, i2) == 3);
        }

        BOOST_AUTO_TEST_CASE(begin_const_)
        {
            using T = unique_array<int>;

            T const a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.begin()), T::const_iterator>);

            auto it = a1.begin();

            BOOST_TEST(*it == 1);
        }

        BOOST_AUTO_TEST_CASE(end_const_)
        {
            using T = unique_array<int>;

            T const a1 { 1, 2, 3 };

            auto i1 = a1.begin();
            auto i2 = a1.end();

            static_assert(std::same_as<decltype(a1.end()), T::const_iterator>);

            BOOST_TEST(std::distance(i1, i2) == 3);
        }

    BOOST_AUTO_TEST_SUITE_END() // iterator_

    BOOST_AUTO_TEST_CASE(front_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.front()), int&>);

        BOOST_TEST(a1.front() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.back()), int&>);

        BOOST_TEST(a1.back() == 3);
    }

    BOOST_AUTO_TEST_CASE(front_const_)
    {
        using T = unique_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.front()), int const&>);

        BOOST_TEST(a1.front() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_const_)
    {
        using T = unique_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.back()), int const&>);

        BOOST_TEST(a1.back() == 3);
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.at(0)), int&>);

        BOOST_TEST(a1.at(1) == 2);
    }

    BOOST_AUTO_TEST_CASE(at_error_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_CHECK_EXCEPTION(
            a1.at(3),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(find_int64(e.context(), "pos") == 3);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(at_const_)
    {
        using T = unique_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.at(0)), int const&>);

        BOOST_TEST(a1.at(1) == 2);
    }

    BOOST_AUTO_TEST_CASE(subscript_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1[0]), int&>);

        BOOST_TEST(a1[1] == 2);
    }

    BOOST_AUTO_TEST_CASE(subscript_const_)
    {
        using T = unique_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1[0]), int const&>);

        BOOST_TEST(a1[1] == 2);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_TEST(a1.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        using T = unique_array<int>;

        T a1;

        BOOST_TEST(a1.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_TEST(!a1.empty());
    }

    BOOST_AUTO_TEST_SUITE(insert_)

        BOOST_AUTO_TEST_CASE(copy_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };
            int i1 = 5;

            auto it1 = a1.insert(a1.begin(), i1);

            BOOST_TEST(a1.size() == 4);
            BOOST_TEST(a1.front() == 5);
            BOOST_CHECK(it1 == a1.begin());
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            auto it1 = a1.insert(a1.begin(), 5);

            BOOST_TEST(a1.size() == 4);
            BOOST_TEST(a1.front() == 5);
            BOOST_CHECK(it1 == a1.begin());
        }

        BOOST_AUTO_TEST_CASE(copy_count_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };
            int i1 = 5;

            auto it1 = a1.insert(a1.begin(), 3, i1);

            auto v = json::value_from(a1);
            json::array expected { 5, 5, 5, 1, 2, 3, };
            BOOST_TEST(v == expected);
            BOOST_CHECK(it1 == a1.begin());
        }

        BOOST_AUTO_TEST_CASE(iterator_range_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };
            std::vector<int> v1 { 4, 5, 6 };

            auto it1 = a1.insert(a1.end(), v1.begin(), v1.end());

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 4, 5, 6 };
            BOOST_TEST(v == expected);
            BOOST_CHECK(it1 == a1.begin() + 3);
        }

        BOOST_AUTO_TEST_CASE(initializer_list_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            auto it1 = a1.insert(a1.end(), { 4, 5, 6});

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 4, 5, 6 };
            BOOST_TEST(v == expected);
            BOOST_CHECK(it1 == a1.begin() + 3);
        }

    BOOST_AUTO_TEST_SUITE_END() // insert_

    BOOST_AUTO_TEST_CASE(emplace_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto it1 = a1.emplace(a1.end(), 4);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(it1 == a1.begin() + 3);
    }

    BOOST_AUTO_TEST_CASE(push_back_copy_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };
        int i1 = 4;

        auto& r = a1.push_back(i1);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(r == 4);
    }

    BOOST_AUTO_TEST_CASE(push_back_move_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto& r = a1.push_back(4);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(r == 4);
    }

    BOOST_AUTO_TEST_CASE(push_back_unique_ptr_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto& r = a1.push_back(std::make_unique<int>(4));

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(r == 4);
    }

    BOOST_AUTO_TEST_CASE(emplace_back_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto& r = a1.emplace_back(4);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(r == 4);
    }

    BOOST_AUTO_TEST_CASE(emplace_back_polymorphic_)
    {
        struct foo
        {
            int v;

            foo(int v) : v { v } {}
            virtual ~foo() = default;
        };

        struct bar : foo
        {
            int w;
            bar(int v) : foo { v } {}
        };

        using T = unique_array<foo>;

        T a1;

        auto& r1 = a1.emplace_back(1);
        static_assert(std::same_as<decltype(r1), foo&>);
        BOOST_TEST(r1.v == 1);

        auto& r2 = a1.emplace_back<bar>(2);
        static_assert(std::same_as<decltype(r2), bar&>);
        BOOST_TEST(r2.v == 2);
    }

    BOOST_AUTO_TEST_SUITE(assign_)

        BOOST_AUTO_TEST_CASE(copy_count_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.assign(5, 1);

            auto v = json::value_from(a1);
            json::array expected { 1, 1, 1, 1, 1, };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(iterator_range_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };
            std::vector<int> v1 { 4, 5, 6 };

            a1.assign(v1.begin(), v1.end());

            auto v = json::value_from(a1);
            json::array expected { 4, 5, 6 };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(initializer_list_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.assign({ 4, 5, 6, });

            auto v = json::value_from(a1);
            json::array expected { 4, 5, 6 };
            BOOST_TEST(v == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // assign_

    BOOST_AUTO_TEST_SUITE(erase_)

        BOOST_AUTO_TEST_CASE(single_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.erase(a1.begin() + 1);

            auto v = json::value_from(a1);
            json::array expected { 1, 3 };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.erase(a1.begin(), a1.end());

            auto v = json::value_from(a1);
            json::array expected { };
            BOOST_TEST(v == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // erase_

    BOOST_AUTO_TEST_SUITE(resize_)

        BOOST_AUTO_TEST_CASE(type_1_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.resize(5);

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 0, 0, };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(type_2_)
        {
            using T = unique_array<int>;

            T a1 { 1, 2, 3 };

            a1.resize(5, 9);

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 9, 9, };
            BOOST_TEST(v == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // resize_

    BOOST_AUTO_TEST_CASE(equal_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };
        T a2 { 1, 2, 3 };
        T a3 { 1 };

        BOOST_CHECK(a1 == a2);
        BOOST_CHECK(a1 != a3);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };
        T a2 { 1, 2, 4 };

        BOOST_CHECK(a1 < a2);
        BOOST_CHECK(a2 > a1);
    }

    BOOST_AUTO_TEST_CASE(adl_erase_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto r = erase(a1, 2);

        auto v = json::value_from(a1);
        json::array expected { 1, 3, };
        BOOST_TEST(v == expected);
        BOOST_TEST(r == 1);
    }

    BOOST_AUTO_TEST_CASE(adl_erase_if_)
    {
        using T = unique_array<int>;

        T a1 { 1, 2, 3 };

        auto r = erase_if(a1, [](auto& v) { return v > 1; });

        auto v = json::value_from(a1);
        json::array expected { 1, };
        BOOST_TEST(v == expected);
        BOOST_TEST(r == 2);
    }

    BOOST_AUTO_TEST_CASE(deduction_guide_)
    {
        std::vector<int> v1 { 1, 2, 3 };

        unique_array a1(v1.begin(), v1.end());

        auto v2 = json::value_from(a1);
        json::array expected { 1, 2, 3 };
        BOOST_TEST(v2 == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // unique_array_

} // namespace testing
